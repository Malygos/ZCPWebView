//
//  WKWebViewJavascriptBridge.h
//
//  Created by @LokiMeyburg on 10/15/14.
//  Copyright (c) 2014 @LokiMeyburg. All rights reserved.
//

#if (__MAC_OS_X_VERSION_MAX_ALLOWED > __MAC_10_9 || __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_7_1)
#define supportsWKWebView
#endif

#if defined supportsWKWebView

#import <Foundation/Foundation.h>
#import <WebKit/WebKit.h>

/**
 html内嵌代码：
 1.定义setupWebViewJavascriptBridge方法
 2.执行setupWebViewJavascriptBridge方法
    1.首次初始化，WebViewJavascriptBridge没值、WVJBCallbacks没值
    2.初始化WVJBCallbacks，并现将参数callback临时保存下来
    3.创建iframe发送https://__bridge_loaded__，webview delegate 拦截到请求后初始化js（injectJavascriptFile），注入WebViewJavascriptBridge_JS中的js代码
 此处的setup函数的定义和执行的设计，是为了确保在callback内bridge参数是一定有值的。因为callback执行的时机在setup函数内做了判断，当bridge有值则执行callback，当bridge没值且WVJBCallbacks有值则先保存下来，后续JS注入时会初始化bridge然后执行callback。如果bridge和WVJBCallbacks均没值，则自己初始化WVJBCallbacks并将callback保存下来，等待后续JS注入执行。
 
 
 注入的JS代码：
 1.初始化WebViewJavascriptBridge
 2.发送https://__wvjb_queue_message__，webview delegate 拦截到请求后走bridge的_fetchQueue方法（首次执行）。
 这次请求好像没什么乱用，但是其实是为了创建真正后续要用的iframe，变量为messagingIframe，后续使用的话就直接使用messagingIframe.src ，并没有使用html内嵌的那个iframe ，所以内嵌的那个请求发送后就被移除。（其实如果真获取的话也能拿得到那个iframe，可能是需要加id之类的标记获取会有存在冲突的可能？最可能的原因是不确定真正注入的先后顺序，有可能html没加载出来时，js就已经注入了，但是注入代码其实可以加个if判断一下）
 3.执行 WVJBCallbacks 中保存的 callback （来自html内嵌代码），此时 js 定义的 call 和 register 被初始化
 
 Native的处理：
 1.如果在 JS 注入之前就有 Native 端执行 callHandler ，那么 js 肯定不会成功响应。所以定义了startupMessageQueue来保存JS注入之前的 callHandler ，当JS注入成功后，再将startupMessageQueue中的信息拿出来执行，以防止上述情况发生。
 

 
 
 
 
 
 
 Native端调用JS方法：
 
 1.JS端的注册
 调用 registerHandler 方法，将 handlerName:handler 键值对保存在 messageHandlers  字典中。
 
 2.Native端的调用
 调用 callHandler 方法，将格式化好的 native message 拼装成JS方法调用，即： WebViewJavascriptBridge._handleMessageFromObjC('%@')。
 其中 callback 会生成对应的callbackId作为message，而callback会存入 responseCallbacks 中。
 然后UIWebView使用 stringByEvaluatingJavaScriptFromString: 执行；WKWebView使用 evaluateJavaScript:completionHandler: 执行。
 其中 message 结构：{'data': data, 'handlerName': 'name', 'callbackId': 'objc_cb_0'}
 
 3.JS端的响应
 _handleMessageFromObjC 方法做了响应处理，根据 handlerName 从 messageHandlers 中拿出 handler执行，
 
 4.JS端的回调
 在上一步中封装好handler的responseCallback参数，该参数实际执行了_doSend方法，_doSend 方法执行的参数为data（即为{handlerName:name, responseId: id, responseData: data}，其中的responseId使用了 native message中的 callbackId，responseData为回调的传参）
 _doSend 方法的实现，将 js message 放到 sendMessageQueue 数组中，然后 请求 https://__wvjb_queue_message__
 
 5.Native端接收回调
 native端 webview delegate 拦截到 __wvjb_queue_message__ 后，使用webview的js调用方法，调用 js 的 _fetchQueue方法获取 js message。
 首先解析responseId，发现有值，根据responseId 从 responseCallbacks 中取出callback 执行。
 
 ps：需要注意的是，_fetchQueue获取到的message在json转model之后是一个数组，flushMessageQueue方法处理了这个messages，使用了 for (WVJBMessage* message in messages)，去遍历处理。
 这里涉及到一个设计问题，这里的设计是：js通过发送https://__wvjb_queue_message__来通知Native有回调，然后native主动调用js方法_fetchQueue获取回调数据。这就导致了回调和回调数据是两步操作，不能同时到达native。所以就产生一个问题，在第一个回调和获取回调数据中间的这段时间，如果又有一个新的回调产生，多个回调数据该如何保存。
 1.考虑保存为{id:data, ...}字典，但是js发送的通知是无信息的，所以native是不知道该获取哪个responseId 对应的回调数据。所以行不通，或者是可以将https://__wvjb_queue_message__ 设计成带参请求。
 2.考虑保存为[data, ...]数组，因为 data中包含 responseId信息，所以可以在native 获取回调数据时一次性返回。
 
 
 
 
 
 
 JS端调用Native方法：
 
 1.Native端的注册
 将 handlerName:handler 键值对放入 messageHandlers 中。

 2.JS端的调用
 JS端调用callHandler方法，实际调用了_doSend方法，_doSend 内部生成 callbackId，将 js message保存到 sendMessageQueue中，然后请求 https://__wvjb_queue_message__
 
 3.Native端的响应
 native端 webview delegate 拦截到 __wvjb_queue_message__ 后，使用webview的js调用方法，调用 js 的 _fetchQueue方法获取 js message。
 首先解析responseId，发现没值，确定不是回调响应而是请求响应。根据handlerName拿到handler执行。
 
 4.Native端的回调
 handler中执行回调block，将callbackId作为responseId和data一起生成message，执行_handleMessageFromObjC
 
 5.JS端接收回调
 JS端 _handleMessageFromObjC 方法中解析出 responseId ，并根据responseId 从responseCallbacks 中拿到callback执行。
 
 
 写JS需要注意：
 分号;最好别省略
 
 */

typedef void (^WVJBResponseCallback)(id responseData);
typedef void (^WVJBHandler)(id data, WVJBResponseCallback responseCallback);

@interface WKWebViewJavascriptBridge : NSObject<WKNavigationDelegate, WKScriptMessageHandler>

/// 如果不想通过注册的方式，那么可以在该对象的类中实现js_#HandlerName#:responseCallback:方法
@property (nonatomic, weak) id scriptMethodResponder;

+ (instancetype)bridgeForWebView:(WKWebView *)webView;
- (instancetype)initForWebView:(WKWebView *)webView;

- (void)registerHandler:(NSString *)handlerName handler:(WVJBHandler)handler;
- (void)removeHandler:(NSString *)handlerName;
- (void)callHandler:(NSString *)handlerName;
- (void)callHandler:(NSString *)handlerName data:(id)data;
- (void)callHandler:(NSString *)handlerName data:(id)data responseCallback:(WVJBResponseCallback)responseCallback;

+ (void)enableLogging;
- (void)reset;
- (void)disableJavscriptAlertBoxSafetyTimeout;
- (void)injectJavascriptFile;

@end

#endif
