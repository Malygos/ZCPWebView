//
//  main.m
//  ZCPWebViewExample
//
//  Created by 朱超鹏(平安健康互联网健康研究院) on 2020/7/17.
//  Copyright © 2020 朱超鹏(平安健康互联网健康研究院). All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    NSString * appDelegateClassName;
    @autoreleasepool {
        // Setup code that might create autoreleased objects goes here.
        appDelegateClassName = NSStringFromClass([AppDelegate class]);
    }
    return UIApplicationMain(argc, argv, nil, appDelegateClassName);
}
