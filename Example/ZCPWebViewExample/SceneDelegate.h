//
//  SceneDelegate.h
//  ZCPWebViewExample
//
//  Created by 朱超鹏(平安健康互联网健康研究院) on 2020/7/17.
//  Copyright © 2020 朱超鹏(平安健康互联网健康研究院). All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

