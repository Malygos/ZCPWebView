//
//  ZCPBaseWebViewController.m
//  ZCPKit
//
//  Created by zhuchaopeng on 16/9/18.
//  Copyright © 2016年 zcp. All rights reserved.
//

#import "ZCPBaseWebViewController.h"
#import "WKWebView+Cookie.h"
#import <ZCPCategory/ZCPCategory.h>
#import <AssertMacros.h>

@interface ZCPBaseWebViewController () <WKUIDelegate, UIGestureRecognizerDelegate>

/// 是否首次加载
@property (nonatomic, assign) BOOL isFirstTimeLoaded;
/// js回调方法名
@property (nonatomic, copy)   NSString *callBackFuncName;
/// url中包含的参数
@property (nonatomic, strong) NSMutableDictionary  *urlParams;
/// 访问地址
@property (nonatomic, copy, readwrite) NSString *urlString;

// FIXME:单独一个分类，需要解决外部传参的兼容问题
/// 分享消息
/// 格式为：{title: "hello", message: "hello", link: "", imageURL: ""}
@property (nonatomic, strong) NSDictionary  *shareDic;
@property (nonatomic, strong) UIImage *shareImage;
@property (nonatomic, copy) NSString *shareLink;

@end

@implementation ZCPBaseWebViewController

// ----------------------------------------------------------------------
#pragma mark - init
// ----------------------------------------------------------------------

- (instancetype)initWithQuery:(NSDictionary *)query {
    if (self = [super init]) {
        // 参数解析
        if (!query || ![query isKindOfClass:[NSDictionary class]]) {
            return self;
        }
        NSString *url = [query objectForKey:@"_url"];
        _urlString = url;
        _urlParams = [url getURLParams].mutableCopy;
        
        NSString *title = [query objectForKey:@"_title"];
        self.title = title;
        
        self.delegate = [query objectForKey:@"_delegate"];
        
        NSString *shareMessage = [query objectForKey:@"_shareMessage"];
        if (shareMessage.length > 0) {
            [self.urlParams setObject:shareMessage forKey:@"_shareMessage"];
        }
    }
    return self;
}

- (instancetype)initWithURLString:(NSString *)urlString {
    if (self = [super init]) {
        if (urlString && [urlString isKindOfClass:[NSString class]]) {
            self.urlString  = urlString;
            self.urlParams  = [urlString getURLParams].mutableCopy;
        }
    }
    return self;
}

// ----------------------------------------------------------------------
#pragma mark - life cycle
// ----------------------------------------------------------------------

- (void)loadView {
    [super loadView];
    [self.view addSubview:self.webView];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initLoadingRightBarButton];
    self.isFirstTimeLoaded = YES;
    
    // FIXME:对外暴露open开关，控制是否使用
    self.jsBridge = [WKWebViewJavascriptBridge bridgeForWebView:self.webView];
    self.jsBridge.scriptMethodResponder = self;
    [self.jsBridge injectJavascriptFile];
    [self registerJSAPI];
    // Cookie
    [self.webView injectCookie];
    
    // 在开始加载url之前要先做好初始化的工作
    [self loadURL:[NSURL URLWithString:self.urlString]];
    
    
    
    // 预加载分享图片
    //    if ([self.urlParams objectForKey:@"_shareMessage"]){
    //        NSString *shareMessage = [self.urlParams objectForKey:@"_shareMessage"];
    //        self.shareDic = [shareMessage JSONObject];
    //        [self preDownloadTargetSharingImageUrl:[self.shareDic objectForKey:@"sImg"]];
    //    }
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    self.webView.frame = self.view.bounds;
}

- (void)dealloc {
    [self.webView.configuration.userContentController removeAllUserScripts];
    if (_webView.observationInfo) {
        [_webView removeObserver:self forKeyPath:@"title"];
    }
    _webView.navigationDelegate = nil;
    _webView.UIDelegate = nil;
    _webView.scrollView.delegate = nil;
    _webView = nil;
}

// ----------------------------------------------------------------------
#pragma mark - load
// ----------------------------------------------------------------------

- (void)loadURLString:(NSString *)urlString {
    self.urlString = urlString;
    self.urlParams = [urlString getURLParams].mutableCopy;
    NSURL *URL = [NSURL URLWithString:urlString];
    [self loadURL:URL];
}

- (void)loadURL:(NSURL *)URL {
    NSTimeInterval timeout = 30;
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:timeout];
    request.HTTPShouldHandleCookies = YES;
    request.mainDocumentURL = URL;
    [self.webView loadRequest:request];
}

- (void)reload {
    self.isFirstTimeLoaded = YES;
    [self.webView reload];
}

// ----------------------------------------------------------------------
#pragma mark - override
// ----------------------------------------------------------------------

- (void)backTo {
    if (self.delegate && [self.delegate respondsToSelector:@selector(webViewDidClickBackBarButton)]) {
        [self.delegate webViewDidClickBackBarButton];
        return;
    }
    if ([self.navigationController respondsToSelector:@selector(popViewControllerAnimated:)]) {
        [self.navigationController popViewControllerAnimated:YES];
    } else if ([self.tabBarController.navigationController respondsToSelector:@selector(popViewControllerAnimated:)]) {
        [self.tabBarController.navigationController popViewControllerAnimated:YES];
    } else {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAll;
}

// ----------------------------------------------------------------------
#pragma mark - Need Subclass Override
// ----------------------------------------------------------------------

- (void)registerJSAPI {
    
}

// ----------------------------------------------------------------------
#pragma mark - private
// ----------------------------------------------------------------------

- (void)openUnconventionalURL:(NSURL *)url {
    
//    NSString *urlString = url.absoluteString;
//
//    // 处理tel url
//    if ([url.scheme isEqualToString:@"tel"]) {
//        openURL(urlString);
//        return;
//    }
//
//    // 处理app url
//    if ([ZCPURLHelper isAppURL:url]) {
//        openURL(urlString);
//    } else {
//        openURL(urlString);
//    }
    
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(openUnconventionalURL:)]) {
        [self.delegate openUnconventionalURL:url];
    }
}

// ----------------------------------------------------------------------
#pragma mark - event response
// ----------------------------------------------------------------------

- (void)clickBackBarButton:(id)sender {
    if ([self.webView canGoBack]) {
        [self.webView goBack];
    } else {
        [self backTo];
    }
}

- (void)clickCloseBarButton:(id)sender {
    [self backTo];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
    if ([keyPath isEqualToString:@"title"]) {
        self.title = [change objectForKey:NSKeyValueChangeNewKey];
    }
}

#pragma mark - WKNavigationDelegate

/**
 在加载请求前调用。用来决定是继续加载请求还是取消。目前先支持子类重写，如果某一步有需求的话就加回调方法处理。

 @param webView 调用该代理方法的WebView
 @param navigationAction 请求相关的描述信息
 @param decisionHandler 需要执行该block来决定是加载还是取消
 */
- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler {
    /**
     WKFrameInfo：
        mainFrame：是主frame还是子frame（如iframe）
        securityOrigin：
     */
    NSURL *URL = navigationAction.request.URL;
    
    // BackForward
    if (navigationAction.navigationType == WKNavigationTypeBackForward) {
        decisionHandler(WKNavigationActionPolicyAllow);
        return;
    }
    // 非web url
    if (![URL isWebURL]) {
        [self openUnconventionalURL:navigationAction.request.URL];
        decisionHandler(WKNavigationActionPolicyCancel);
        return;
    }
    // 如果首次加载且有cookie
    if ((navigationAction.navigationType == WKNavigationTypeLinkActivated || self.isFirstTimeLoaded)
        && ![navigationAction.request.allHTTPHeaderFields.allKeys containsObject:@"Cookie"]) {
        NSMutableURLRequest *request = navigationAction.request.mutableCopy;
        self.isFirstTimeLoaded = NO;
        [request setValue:[self.webView getHeaderCookie] forHTTPHeaderField:@"Cookie"];
        request.HTTPShouldHandleCookies = YES;
        [self.webView loadRequest:request];
        decisionHandler(WKNavigationActionPolicyCancel);
        return;
    }
    // 允许加载请求
    decisionHandler(WKNavigationActionPolicyAllow);
}

/**
 开始加载WebContent时调用

 @param webView 调用该代理方法的WebView
 @param navigation WKNavigation
 */
- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(null_unspecified WKNavigation *)navigation {
    [self clearNavigationBar];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
}

/**
 收到服务器的响应头后调用。决定是继续接收内容还是取消。

 @param webView 调用该代理方法的WebView
 @param navigationResponse 响应信息
 @param decisionHandler 需要执行该block来决定是继续还是取消
 */
- (void)webView:(WKWebView *)webView decidePolicyForNavigationResponse:(WKNavigationResponse *)navigationResponse decisionHandler:(void (^)(WKNavigationResponsePolicy))decisionHandler {
    /**
     MIMEType：Content-Type字段值的类型，比如text/html（超文本标记语言文本）、image/gif（GIF图形）等
     */
    decisionHandler(WKNavigationResponsePolicyAllow);
}

/**
 接收到服务端重定向请求后调用

 @param webView 调用该代理方法的WebView
 @param navigation WKNavigation
 */
- (void)webView:(WKWebView *)webView didReceiveServerRedirectForProvisionalNavigation:(null_unspecified WKNavigation *)navigation {
    NSURL *redirectURL = webView.URL;
    if (![redirectURL isWebURL]) {
        [self openUnconventionalURL:redirectURL];
        return;
    }
    NSMutableURLRequest *request = [NSMutableURLRequest new];
    self.isFirstTimeLoaded = NO;
    request.URL = redirectURL;
    [request setValue:[self.webView getHeaderCookie] forHTTPHeaderField:@"Cookie"];
    request.HTTPShouldHandleCookies = YES;
    [self.webView loadRequest:request];
}

/**
 开始接收WebContent内容时调用

 @param webView 调用该代理方法的WebView
 @param navigation WKNavigation
 */
- (void)webView:(WKWebView *)webView didCommitNavigation:(WKNavigation *)navigation {
}

/**
 请求完成时调用

 @param webView 调用该代理方法的WebView
 @param navigation WKNavigation
 */
- (void)webView:(WKWebView *)webView didFinishNavigation:(null_unspecified WKNavigation *)navigation {
    self.title = webView.title;
    [self configureNavigationBar];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
}

/**
 请求失败时调用

 @param webView 调用该代理方法的WebView
 @param navigation WKNavigation
 @param error 错误信息
 */
- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(null_unspecified WKNavigation *)navigation withError:(NSError *)error {
    // Ignore NSURLErrorDomain error -999.
    if([error code] == NSURLErrorCancelled) {
        return;
    }
    // Ignore "Fame Load Interrupted" errors. Seen after app store links.
    if (error.code == 102 && [error.domain isEqual:@"WebKitErrorDomain"]) {
        return;
    }
    // Show exception view
    if ([error code] == NSURLErrorNotConnectedToInternet) {
    } else {
    }
}

/**
 请求失败时调用（？）

 @param webView 调用该代理方法的WebView
 @param navigation WKNavigation
 @param error 错误信息
 */
- (void)webView:(WKWebView *)webView didFailNavigation:(null_unspecified WKNavigation *)navigation withError:(NSError *)error {
    // Ignore NSURLErrorDomain error -999.
    if ([error code] == NSURLErrorCancelled) {
        return;
    }
    // Ignore "Fame Load Interrupted" errors. Seen after app store links.
    if (error.code == 102 && [error.domain isEqual:@"WebKitErrorDomain"]) {
        return;
    }
    [self clearNavigationBar];
    self.title = @"加载失败";
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
}

/**
 需要进行身份验证质询时调用。客户端为了进行认证，需要使用服务端期望的认证信息创建一个NSURLCredential对象。
 authenticationMethod的值表示服务端期望的认证方式。如：
    NSURLAuthenticationMethodHTTPBasic（HTTP基本认证），需要客户端提供用户名密码，提示用户输入必要信息并使用 credentialWithUser:password:persistence: 方法创建一个NSURLCredential 对象。
    NSURLAuthenticationMethodHTTPDigest（HTTP数字认证），同上。
    NSURLAuthenticationMethodClientCertificate（客户端证书认证），需要系统标识和所有服务端认证所需要的证书，使用 credentialWithIdentity:certificates:persistence:来创建一个NSURLCredential对象。
    NSURLAuthenticationMethodServerTrust（服务端信任认证），需要一个由认证请求的保护空间提供的信任。使用 credentialForTrust:来创建一个NSURLCredential对象。
    等。
 
 NSURLSessionAuthChallengeDisposition的值：

 @param webView 调用该代理方法的WebView
 @param challenge 身份验证质询对象
 @param completionHandler 处理block
 */
- (void)webView:(WKWebView *)webView didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition, NSURLCredential * _Nullable))completionHandler {
    
    NSLog(@"challenge:\n%@", @{@"protectionSpace": challenge.protectionSpace?:@"",
                               @"proposedCredential": challenge.proposedCredential?:@"",
                               @"previousFailureCount": @(challenge.previousFailureCount),
                               @"failureResponse": challenge.failureResponse?:@"",
                               @"error": challenge.error?:@"",
                               @"sender": challenge.sender
                               });
    
    if ([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust]) {
        SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
        
        NSString *domain = challenge.protectionSpace.host;
        BOOL validatesDomainName = YES;
        BOOL allowInvalidCertificates = NO;
        
        NSMutableArray *policies = [NSMutableArray array];
        if (validatesDomainName) {
            [policies addObject:(__bridge_transfer id)SecPolicyCreateSSL(true, (__bridge CFStringRef)domain)];
        } else {
            [policies addObject:(__bridge_transfer id)SecPolicyCreateBasicX509()];
        }
        SecTrustSetPolicies(serverTrust, (__bridge CFArrayRef)policies);
        
        if (allowInvalidCertificates || AFServerTrustIsValid(serverTrust)) {
            CFDataRef exceptions = SecTrustCopyExceptions(serverTrust);
            SecTrustSetExceptions(serverTrust, exceptions);
            CFRelease(exceptions);
            NSURLCredential *credential = [NSURLCredential credentialForTrust:serverTrust];
            if (credential) {
                completionHandler(NSURLSessionAuthChallengeUseCredential, credential);
            } else {
                completionHandler(NSURLSessionAuthChallengePerformDefaultHandling, nil);
            }
        } else {
            completionHandler(NSURLSessionAuthChallengePerformDefaultHandling, nil);
        }
    } else {
        completionHandler(NSURLSessionAuthChallengePerformDefaultHandling, nil);
    }
}

/**
 WebContent Process终止时调用。可能是由于WKWebView内存占用太大导致WebContent Process Crash。

 @param webView WKWebView
 */
- (void)webViewWebContentProcessDidTerminate:(WKWebView *)webView {
    [webView reload];
    NSLog(@"");
}

static BOOL AFServerTrustIsValid(SecTrustRef serverTrust) {
    BOOL isValid = NO;
    SecTrustResultType result;
    __Require_noErr_Quiet(SecTrustEvaluate(serverTrust, &result), _out);
    
    isValid = (result == kSecTrustResultUnspecified || result == kSecTrustResultProceed);
    
_out:
    return isValid;
}

// ----------------------------------------------------------------------
#pragma mark - WKUIDelegate
// ----------------------------------------------------------------------

#pragma mark <对话框>

/**
 显示JS警告对话框。对应js的"alert(string)"函数

 @param webView WKWebView
 @param message 对话框中的提示信息
 @param frame JSProcess启动时所属的Frame信息
 @param completionHandler 对话框Dismiss时的回调
 */
- (void)webView:(WKWebView *)webView runJavaScriptAlertPanelWithMessage:(NSString *)message initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(void))completionHandler {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:message message:nil preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        completionHandler();
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
}

/**
 显示JS确认对话框。对应js的"bool confirm(string)"函数

 @param webView WKWebView
 @param message 对话框中的提示信息
 @param frame JSProcess启动时所属的Frame信息
 @param completionHandler 对话框Dismiss时的回调，带一个Bool参数。
 */
- (void)webView:(WKWebView *)webView runJavaScriptConfirmPanelWithMessage:(NSString *)message initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(BOOL))completionHandler {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:message message:nil preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        completionHandler(YES);
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        completionHandler(NO);
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
}

/**
 显示JS输入对话框。对应js的"prompt(string,string)"函数

 @param webView WKWebView
 @param prompt 对话框中的提示信息
 @param defaultText 对话框中输入框的默认文本内容
 @param frame JSProcess启动时所属的Frame信息
 @param completionHandler 对话框Dismiss时的回调，带一个NSString参数。
 */
- (void)webView:(WKWebView *)webView runJavaScriptTextInputPanelWithPrompt:(NSString *)prompt defaultText:(NSString *)defaultText initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(NSString * _Nullable))completionHandler {
    // 可以根据需要实现
    completionHandler(@"Client Not handler");
}

#pragma mark <预览链接相关 iOS10及以后可用>
- (BOOL)webView:(WKWebView *)webView shouldPreviewElement:(WKPreviewElementInfo *)elementInfo  API_AVAILABLE(ios(10.0)) {
    return YES;
}

- (UIViewController *)webView:(WKWebView *)webView previewingViewControllerForElement:(WKPreviewElementInfo *)elementInfo defaultActions:(NSArray<id<WKPreviewActionItem>> *)previewActions  API_AVAILABLE(ios(10.0)) {
    return nil;
}

- (void)webView:(WKWebView *)webView commitPreviewingViewController:(UIViewController *)previewingViewController {
}

#pragma mark <window行为>

/**
 创建一个新的webView。对应window.open(URL,name,specs,replace)方法
 
 @param webView WKWebView
 @param configuration 用与创建新webView的配置信息
 @param navigationAction 使新webView被创建的导航信息
 @param windowFeatures window信息
 @return 新的webview
 */
- (WKWebView *)webView:(WKWebView *)webView createWebViewWithConfiguration:(WKWebViewConfiguration *)configuration forNavigationAction:(WKNavigationAction *)navigationAction windowFeatures:(WKWindowFeatures *)windowFeatures {
    return nil;
}

/**
 当HTML DOM 执行close()方法也就是关闭浏览器窗口时调用。对应window.close()方法。iOS9及以后可用
 
 @param webView WKWebView
 */
- (void)webViewDidClose:(WKWebView *)webView {
}

// ----------------------------------------------------------------------
#pragma mark - getters and setters
// ----------------------------------------------------------------------

- (WKWebView *)webView {
    if (!_webView) {
        WKWebViewConfiguration *configuration = [[WKWebViewConfiguration alloc] init];
        // 设置偏好设置
        configuration.preferences = [[WKPreferences alloc] init];
        // 默认为0
        configuration.preferences.minimumFontSize = 10;
        // 默认认为YES
        configuration.preferences.javaScriptEnabled = YES;
        // 在iOS上默认为NO，表示不能自动通过窗口打开
        configuration.preferences.javaScriptCanOpenWindowsAutomatically = NO;
        // 注入JS对象名称AppModel，当JS通过AppModel来调用时，
        // 我们可以在WKScriptMessageHandler代理中接收到
        // 通过JS与webview内容交互
        configuration.userContentController = [WKUserContentController new];
        configuration.selectionGranularity = WKSelectionGranularityCharacter;
        // web内容处理池，由于没有属性可以设置，也没有方法可以调用，不用手动创建
        configuration.processPool = [PAWKSharedProcessPool sharedInstance];
        
        _webView = [[WKWebView alloc] initWithFrame:CGRectZero configuration:configuration];
        _webView.navigationDelegate = self;
        _webView.UIDelegate = self;
        [_webView addObserver:self forKeyPath:@"title" options:NSKeyValueObservingOptionPrior context:NULL];
        if (@available(iOS 11.0, *)) {
            // iOS11 以后加载本地页面引起内容向下偏移问题
            _webView.scrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
        if (@available(iOS 9.0, *)) {
            _webView.allowsLinkPreview = NO;
        }
    }
    return _webView;
}

@end


@implementation ZCPBaseWebViewController (NavigationBar)

- (void)clearNavigationBar {
    self.navigationItem.rightBarButtonItem = nil;
}

- (void)initLoadingRightBarButton {
    UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithCustomView:indicator];
    self.navigationItem.rightBarButtonItem = rightButton;
    [indicator startAnimating];
}

- (void)configureNavigationBar {
    UIBarButtonItem *backItem   = [UIBarButtonItem setBackItemWithTarget:self action:@selector(clickBackBarButton:)];
    UIBarButtonItem *closeItem  = [UIBarButtonItem rightBarItemWithTitle:@"X" font:[UIFont systemFontOfSize:24.0f] target:self action:@selector(clickCloseBarButton:)];
    
    if ([self.webView canGoBack]) {
        self.navigationItem.leftBarButtonItems = @[backItem, closeItem];
    } else {
        self.navigationItem.leftBarButtonItems = @[backItem];
    }
}

- (void)initShareBarButton {
    self.navigationItem.rightBarButtonItem = [self generateShareBarItem];
}

- (UIBarButtonItem *)generateShareBarItem {
    UIBarButtonItem *rightButton = [UIBarButtonItem rightBarItemWithTitle:@"Share"
                                                               titleColor:@"333333"
                                                                     font:[UIFont systemFontOfSize:24.0]
                                                                   target:self
                                                                   action:@selector(shareButtonClicked:)];
    return rightButton;
}

- (UIBarButtonItem *)generateMoreBarItem {
    UIBarButtonItem *rightButton = [UIBarButtonItem rightBarItemWithTitle:@"More"
                                                               titleColor:@"333333"
                                                                     font:[UIFont systemFontOfSize:24.0]
                                                                   target:self
                                                                   action:@selector(moreButtonClicked:)];
    return rightButton;
}

#pragma mark - JS triggerd RightBarItem Initializer

//- (void)jsConfiguredLinkItem:(NSString *)jsonStr{
//    NSDictionary *dictionary = [jsonStr JSONObject];
//    if (![dictionary isKindOfClass:[NSDictionary class]]) {
//        return;
//    }
//    self.shareLink = [dictionary objectForKey:@"url"];
//    UIBarButtonItem *rightButton = [UIBarButtonItem rightBarItemWithTitle:[dictionary objectForKey:@"name"]
//                                                                     font:[UIFont systemFontOfSize:15.0]
//                                                                   target:self
//                                                                   action:@selector(openLink)];
//    self.navigationItem.rightBarButtonItem = rightButton;
//}
//
//- (void)jsCongiuredMutipleRightItems:(NSString *)jsonStr{
//    NSDictionary *dictionary = [jsonStr JSONObject];
//    if (![dictionary isKindOfClass:[NSDictionary class]]) {
//        return;
//    }
//    NSMutableArray *items =  [NSMutableArray new];
//
//    if([dictionary objectForKey:@"share"]) {
//        [self configureJSShareDictionary:[dictionary objectForKey:@"share"]];
//        [items addObject:[self generateShareBarItem]];
//    }
//
//    if (items.count > 0) {
//        self.navigationItem.rightBarButtonItems = items;
//    }
//}
//
//- (void)jsTriggerNativeSharePopView:(NSString *)jsonStr{
//    NSDictionary *dictionary = [jsonStr JSONObject];
//    [self configureJSShareDictionary:dictionary];
//    [self shareButtonClicked:nil];
//}
//
//- (void)openLink{
//    openURL(self.shareLink);
//}
//
//- (void)jsConfiguredShareItem:(NSString *)jsonShare{
//    NSDictionary *dictionary = [jsonShare JSONObject];
//    [self configureJSShareDictionary:dictionary];
//    [self initShareBarButton];
//}
//
//- (void)configureJSShareDictionary:(NSDictionary *)dict{
//    if (![dict isKindOfClass:[NSDictionary class]]) {
//        return;
//    }
//    if (![dict objectForKey:@"title"] || ![dict objectForKey:@"desc"] || ![dict objectForKey:@"shareUrl"] || ![dict objectForKey:@"imgUrl"]) {
//        return;
//    }
//    NSMutableDictionary *temDict = [NSMutableDictionary new];
//    [temDict setObject:[dict objectForKey:@"title"] forKey:@"sTitle"];
//    [temDict setObject:[dict objectForKey:@"desc"] forKey:@"sMsg"];
//    [temDict setObject:[dict objectForKey:@"shareUrl"] forKey:@"sUrl"];
//    [temDict setObject:[dict objectForKey:@"imgUrl"] forKey:@"sImg"];
//
//    if ([[dict objectForKey:@"success"] isKindOfClass:[NSString class]]) { // 分享成功 回调方法 名
//        [temDict setObject:[dict objectForKey:@"success"] forKey:@"success"];
//    }
//
//    if ([[dict objectForKey:@"cancel"] isKindOfClass:[NSString class]]) { // 分享取消 回调方法 名
//        [temDict setObject:[dict objectForKey:@"cancel"] forKey:@"cancel"];
//    }
//
//    self.shareDic = temDict;
//    [self preDownloadTargetSharingImageUrl:[self.shareDic objectForKey:@"sImg"]];
//}
//
//- (void)preDownloadTargetSharingImageUrl:(NSString *)imageUrl {
//    if(imgUrl && [NSURL URLWithString:imgUrl]){
//        @weakify(self);
//        SDWebImageManager *manager      = [SDWebImageManager sharedManager];
//        [manager downloadImageWithURL:[NSURL URLWithString:imgUrl]
//                              options:SDWebImageRefreshCached progress:nil
//                            completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
//                                weakself.shareImage         = image;
//                            }];
//    }
//}

// ----------------------------------------------------------------------
#pragma mark - event response
// ----------------------------------------------------------------------
- (void)shareButtonClicked:(id)sender {
}

- (void)moreButtonClicked:(UIButton *)sender{
}

@end
