//
//  WKWebViewJavascriptBridge.m
//
//  Created by @LokiMeyburg on 10/15/14.
//  Copyright (c) 2014 @LokiMeyburg. All rights reserved.
//


#import "WKWebViewJavascriptBridge.h"
#import "WebViewJavascriptBridge_JS.h"

#if defined supportsWKWebView

#define ScriptMessageHandlerName    @"zcpJSCallNative"

typedef NSDictionary WVJBMessage;

@interface WKWebViewJavascriptBridge ()

@property (nonatomic, weak) WKWebView *webView;
@property (nonatomic, assign) long uniqueId;

@property (strong, nonatomic) NSMutableArray *startupMessageQueue;
@property (strong, nonatomic) NSMutableDictionary *responseCallbacks;
@property (strong, nonatomic) NSMutableDictionary *messageHandlers;

@end

@implementation WKWebViewJavascriptBridge

#pragma mark - log

static bool logging = false;
static int logMaxLength = 500;

+ (void)enableLogging { logging = true; }
+ (void)setLogMaxLength:(int)length { logMaxLength = length;}

void ZCPWKWVJBLog(NSString *format, ...) {
    if (!logging) { return; }
    va_list args;
    va_start(args, format);
    NSString *log = [[NSString alloc] initWithFormat:format arguments:args];
    va_end(args);
    NSLog(@"%@", log);
}

void ZCPWKWVJBMessageLog(NSString *action, id messageJSON) {
    if (!logging) { return; }
    if (![messageJSON isKindOfClass:[NSString class]]) {
        messageJSON = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:messageJSON options:(NSJSONWritingOptions)(NSJSONWritingPrettyPrinted) error:nil] encoding:NSUTF8StringEncoding];
    }
    if ([messageJSON length] > logMaxLength) {
        NSLog(@"WVJB %@: %@ [...]", action, [messageJSON substringToIndex:logMaxLength]);
    } else {
        NSLog(@"WVJB %@: %@", action, messageJSON);
    }
}

#pragma mark - life cycle

+ (instancetype)bridgeForWebView:(WKWebView *)webView {
    WKWebViewJavascriptBridge *bridge = [[self alloc] initForWebView:webView];
    return bridge;
}

- (instancetype)initForWebView:(WKWebView *)webView {
    if (self = [super init]) {
        _webView = webView;
        [_webView.configuration.userContentController addScriptMessageHandler:self name:ScriptMessageHandlerName];
        _uniqueId = 0;
        [self reset];
    }
    return self;
}

- (void)dealloc {
    [_webView.configuration.userContentController removeScriptMessageHandlerForName:ScriptMessageHandlerName];
    _webView = nil;
    _startupMessageQueue = nil;
    _responseCallbacks = nil;
    _messageHandlers = nil;
}

#pragma mark - API

- (void)registerHandler:(NSString *)handlerName handler:(WVJBHandler)handler {
    self.messageHandlers[handlerName] = [handler copy];
}

- (void)removeHandler:(NSString *)handlerName {
    [self.messageHandlers removeObjectForKey:handlerName];
}

- (void)callHandler:(NSString *)handlerName {
    [self callHandler:handlerName data:nil responseCallback:nil];
}

- (void)callHandler:(NSString *)handlerName data:(id)data {
    [self callHandler:handlerName data:data responseCallback:nil];
}

- (void)callHandler:(NSString *)handlerName data:(id)data responseCallback:(WVJBResponseCallback)responseCallback {
    [self sendData:data responseCallback:responseCallback handlerName:handlerName];
}

- (void)reset {
    self.startupMessageQueue = [NSMutableArray array];
    self.responseCallbacks = [NSMutableDictionary dictionary];
    _uniqueId = 0;
}

- (void)disableJavscriptAlertBoxSafetyTimeout {
    [self sendData:nil responseCallback:nil handlerName:@"_disableJavascriptAlertBoxSafetyTimeout"];
}

- (void)injectJavascriptFile {
    NSString *js = WebViewJavascriptBridge_js();
    BOOL alreadyExist = NO;
    
    for (WKUserScript *script in self.webView.configuration.userContentController.userScripts) {
        if ([script.source isEqualToString:js]) {
            alreadyExist = YES;
        }
    }
    
    if (!alreadyExist) {
        WKUserScript *userScript = [[WKUserScript alloc] initWithSource:js injectionTime:WKUserScriptInjectionTimeAtDocumentStart forMainFrameOnly:NO];
        [self.webView.configuration.userContentController addUserScript:userScript];
        [self.webView.configuration.userContentController addUserScript:userScript];
    }
    
    if (self.startupMessageQueue) {
        NSArray* queue = self.startupMessageQueue;
        self.startupMessageQueue = nil;
        for (id queuedMessage in queue) {
            [self _dispatchMessage:queuedMessage];
        }
    }
}

#pragma mark - private

- (void)sendData:(id)data responseCallback:(WVJBResponseCallback)responseCallback handlerName:(NSString*)handlerName {
    NSMutableDictionary* message = [NSMutableDictionary dictionary];
    
    if (data) {
        message[@"data"] = data;
    }
    
    if (responseCallback) {
        NSString* callbackId = [NSString stringWithFormat:@"objc_cb_%ld", ++_uniqueId];
        self.responseCallbacks[callbackId] = [responseCallback copy];
        message[@"callbackId"] = callbackId;
    }
    
    if (handlerName) {
        message[@"handlerName"] = handlerName;
    }
    [self _queueMessage:message];
}

- (void)_queueMessage:(WVJBMessage *)message {
    if (self.startupMessageQueue) {
        [self.startupMessageQueue addObject:message];
    } else {
        [self _dispatchMessage:message];
    }
}

- (void)_dispatchMessage:(WVJBMessage*)message {
    NSString *messageJSON = [self _serializeMessage:message pretty:NO];
    ZCPWKWVJBMessageLog(@"SEND", messageJSON);
    messageJSON = [messageJSON stringByReplacingOccurrencesOfString:@"\\" withString:@"\\\\"];
    messageJSON = [messageJSON stringByReplacingOccurrencesOfString:@"\"" withString:@"\\\""];
    messageJSON = [messageJSON stringByReplacingOccurrencesOfString:@"\'" withString:@"\\\'"];
    messageJSON = [messageJSON stringByReplacingOccurrencesOfString:@"\n" withString:@"\\n"];
    messageJSON = [messageJSON stringByReplacingOccurrencesOfString:@"\r" withString:@"\\r"];
    messageJSON = [messageJSON stringByReplacingOccurrencesOfString:@"\f" withString:@"\\f"];
    messageJSON = [messageJSON stringByReplacingOccurrencesOfString:@"\u2028" withString:@"\\u2028"];
    messageJSON = [messageJSON stringByReplacingOccurrencesOfString:@"\u2029" withString:@"\\u2029"];
    
    NSString* javascriptCommand = [NSString stringWithFormat:@"WebViewJavascriptBridge._handleMessageFromObjC('%@');", messageJSON];
    if ([[NSThread currentThread] isMainThread]) {
        [self _evaluateJavascript:javascriptCommand];
    } else {
        dispatch_sync(dispatch_get_main_queue(), ^{
            [self _evaluateJavascript:javascriptCommand];
        });
    }
}

- (void)_evaluateJavascript:(NSString *)javascriptCommand {
    [_webView evaluateJavaScript:javascriptCommand completionHandler:nil];
}

- (void)_flushMessageQueueObject:(id)messageQueueObject{
    WVJBMessage* message = messageQueueObject;
    
    if (![message isKindOfClass:[WVJBMessage class]]) {
        ZCPWKWVJBLog(@"WebViewJavascriptBridge: WARNING: Invalid %@ received: %@", [message class], message);
        return;
    }
    ZCPWKWVJBMessageLog(@"RCVD", message);
    
    NSString* responseId = message[@"responseId"];
    if (responseId) {
        WVJBResponseCallback responseCallback = _responseCallbacks[responseId];
        responseCallback(message[@"responseData"]);
        [self.responseCallbacks removeObjectForKey:responseId];
    } else {
        WVJBResponseCallback responseCallback = NULL;
        NSString* callbackId = message[@"callbackId"];
        if (callbackId) {
            responseCallback = ^(id responseData) {
                if (responseData == nil) {
                    responseData = [NSNull null];
                }
                
                WVJBMessage* msg = @{ @"responseId":callbackId, @"responseData":responseData };
                [self _queueMessage:msg];
            };
        } else {
            responseCallback = ^(id ignoreResponseData) {
                // Do nothing
            };
        }
        
        WVJBHandler handler = self.messageHandlers[message[@"handlerName"]];
        
        if (handler) {
            handler(message[@"data"], responseCallback);
        } else if (self.scriptMethodResponder) {
            SEL sel = NSSelectorFromString([NSString stringWithFormat:@"js_%@:responseCallback:", message[@"handlerName"]]);
            if ([self.scriptMethodResponder respondsToSelector:sel]) {
                #pragma clang diagnostic push
                #pragma clang diagnostic ignored "-Warc-performSelector-leaks"
                [self.scriptMethodResponder performSelector:sel withObject:message[@"data"] withObject:responseCallback];
                #pragma clang diagnostic pop
            }
        } else {
            ZCPWKWVJBLog(@"WVJBNoHandlerException, No handler for message from JS: %@", message);
        }
    }
}

- (NSString *)_serializeMessage:(id)message pretty:(BOOL)pretty {
    return [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:message options:(NSJSONWritingOptions)(pretty ? NSJSONWritingPrettyPrinted : 0) error:nil] encoding:NSUTF8StringEncoding];
}

- (NSArray*)_deserializeMessageJSON:(NSString *)messageJSON {
    return [NSJSONSerialization JSONObjectWithData:[messageJSON dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingAllowFragments error:nil];
}

#pragma mark - WKScriptMessageHandler

- (void)userContentController:(WKUserContentController *)userContentController didReceiveScriptMessage:(WKScriptMessage *)message {
    
    if ([message.name isEqualToString:ScriptMessageHandlerName]) {
        NSDictionary *body = message.body;
        [self _flushMessageQueueObject:body];
    } else {
        // Unknown
        NSLog(@"WebViewJavascriptBridge: WARNING: Received unknown WebViewJavascriptBridge command %@ %@", message.name, message.body);
    }
}

#pragma mark - getters and setters

- (NSMutableDictionary *)messageHandlers {
    if (!_messageHandlers) {
        _messageHandlers = [NSMutableDictionary dictionary];
    }
    return _messageHandlers;
}

@end

#endif
