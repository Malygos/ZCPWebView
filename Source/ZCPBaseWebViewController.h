//
//  ZCPBaseWebViewController.h
//  ZCPKit
//
//  Created by zhuchaopeng on 16/9/18.
//  Copyright © 2016年 zcp. All rights reserved.
//

#import <WebKit/WebKit.h>
#import "WKWebViewJavascriptBridge.h"

// ----------------------------------------------------------------------
#pragma mark - 回调代理协议
// ----------------------------------------------------------------------
@protocol ZCPBaseWebViewControllerDelegate <NSObject>

@optional
/**
 webview返回按钮响应事件
 */
- (void)webViewDidClickBackBarButton;

/**
 判断当前加载的url是否是WKWebView不能打开的协议类型。例如：phone numbers, email address, maps等
 */
- (void)openUnconventionalURL:(NSURL *)URL;

@end

// ----------------------------------------------------------------------
#pragma mark - WebViewController 用来装载网页，并做响应处理
// ----------------------------------------------------------------------
@interface ZCPBaseWebViewController : UIViewController <WKNavigationDelegate>

/// WebView
@property (nonatomic, strong) WKWebView *webView;
/// JSBridge
@property (nonatomic, strong) WKWebViewJavascriptBridge *jsBridge;
/// 访问地址
@property (nonatomic, copy, readonly) NSString *urlString;
/// 回调代理
@property (nonatomic, weak) id<ZCPBaseWebViewControllerDelegate> delegate;

#pragma mark <init>
/// 实例化方法
- (instancetype)initWithURLString:(NSString *)urlString;

#pragma mark <load>
/// 加载url
- (void)loadURLString:(NSString *)urlString;
/// 重新加载
- (void)reload;

#pragma mark <Need Subclass Override>
/// 注册JS方法
- (void)registerJSAPI;

@end



@interface ZCPBaseWebViewController (NavigationBar)

/**
 清除导航内容
 */
- (void)clearNavigationBar;

/**
 初始化右侧加载导航按钮
 */
- (void)initLoadingRightBarButton;

/**
 初始化右侧导航按钮
 */
- (void)configureNavigationBar;

/**
 初始化分享按钮
 */
- (void)initShareBarButton;

//- (void)jsConfiguredLinkItem:(NSString *)jsonStr;
//- (void)jsConfiguredShareItem:(NSString *)jsonShare;
//- (void)jsCongiuredMutipleRightItems:(NSString *)jsonStr;
//- (void)jsTriggerNativeSharePopView:(NSString *)jsonStr;

/**
 预加载分享图片

 @param imageUrl 分享图片链接
 */
//- (void)preDownloadTargetSharingImageUrl:(NSString *)imageUrl;

@end
